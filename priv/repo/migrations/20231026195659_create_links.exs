defmodule Pc.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :sequence, :integer
      add :active, :boolean, default: false, null: false
      add :chain, references(:chains, on_delete: :nothing, type: :binary_id)
      add :from, references(:users, on_delete: :nothing, type: :binary_id)
      add :to, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:links, [:chain])
    create index(:links, [:from])
    create index(:links, [:to])
  end
end
