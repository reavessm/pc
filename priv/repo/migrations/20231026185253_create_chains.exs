defmodule Pc.Repo.Migrations.CreateChains do
  use Ecto.Migration

  def change do
    create table(:chains, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :topics, :text
      add :status, :string
      add :group, references(:groups, on_delete: :nothing, type: :binary_id)
      add :current_link, references(:links, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:chains, [:group])
    create index(:chains, [:current_link])
  end
end
