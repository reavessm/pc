defmodule Pc.Repo.Migrations.CreatePrecants do
  use Ecto.Migration

  def change do
    create table(:precants, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :group, references(:groups, on_delete: :nothing, type: :binary_id)
      add :user, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:precants, [:group])
    create index(:precants, [:user])
    create unique_index(:precants, [:group, :user], name: :user_per_group)
  end
end
