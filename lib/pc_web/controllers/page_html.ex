defmodule PcWeb.PageHTML do
  use PcWeb, :html

  embed_templates "page_html/*"
end
