defmodule PcWeb.Layouts do
  use PcWeb, :html

  embed_templates "layouts/*"
end
