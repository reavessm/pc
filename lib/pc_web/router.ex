defmodule PcWeb.Router do
  use PcWeb, :router

  import PcWeb.UsersAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {PcWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_users
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PcWeb do
    pipe_through :browser

    get "/", PageController, :home

    live "/groups", GroupLive.Index, :index
    live "/groups/new", GroupLive.Index, :new
    live "/groups/:id/edit", GroupLive.Index, :edit

    live "/groups/:id", GroupLive.Show, :show
    live "/groups/:id/show/edit", GroupLive.Show, :edit

    live "/precants", PrecantsLive.Index, :index
    live "/precants/new", PrecantsLive.Index, :new
    live "/precants/:id/edit", PrecantsLive.Index, :edit

    live "/precants/:id", PrecantsLive.Show, :show
    live "/precants/:id/show/edit", PrecantsLive.Show, :edit

    live "/chains", ChainLive.Index, :index
    live "/chains/new", ChainLive.Index, :new
    live "/chains/:id/edit", ChainLive.Index, :edit

    live "/chains/:id", ChainLive.Show, :show
    live "/chains/:id/show/edit", ChainLive.Show, :edit

    live "/links", LinkLive.Index, :index
    live "/links/new", LinkLive.Index, :new
    live "/links/:id/edit", LinkLive.Index, :edit

    live "/links/:id", LinkLive.Show, :show
    live "/links/:id/show/edit", LinkLive.Show, :edit
  end

  # Other scopes may use custom stacks.
  # scope "/api", PcWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:pc, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: PcWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", PcWeb do
    pipe_through [:browser, :redirect_if_users_is_authenticated]

    live_session :redirect_if_users_is_authenticated,
      on_mount: [{PcWeb.UsersAuth, :redirect_if_users_is_authenticated}] do
      live "/users/register", UsersRegistrationLive, :new
      live "/users/log_in", UsersLoginLive, :new
      live "/users/reset_password", UsersForgotPasswordLive, :new
      live "/users/reset_password/:token", UsersResetPasswordLive, :edit
    end

    post "/users/log_in", UsersSessionController, :create
  end

  scope "/", PcWeb do
    pipe_through [:browser, :require_authenticated_users]

    live_session :require_authenticated_users,
      on_mount: [{PcWeb.UsersAuth, :ensure_authenticated}] do
      live "/users/settings", UsersSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UsersSettingsLive, :confirm_email
    end
  end

  scope "/", PcWeb do
    pipe_through [:browser]

    delete "/users/log_out", UsersSessionController, :delete

    live_session :current_users,
      on_mount: [{PcWeb.UsersAuth, :mount_current_users}] do
      live "/users/confirm/:token", UsersConfirmationLive, :edit
      live "/users/confirm", UsersConfirmationInstructionsLive, :new
    end
  end
end
