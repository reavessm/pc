defmodule PcWeb.LinkLive.FormComponent do
  use PcWeb, :live_component

  alias Pc.Chains

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage link records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="link-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <!-- TODO: Limit this to only users in Chain.group -->
        <.input
          field={@form[:chain]}
          type="select"
          label="Chain"
          prompt="Choose a Chain"
          options={
            # Select group and concat c.group.name <> c.name?
            Chains.list_chains()
            |> Enum.map(fn c -> {c.name, c.id} end)
            |> Enum.to_list()
          }
        />
        <.input
          field={@form[:from]}
          type="select"
          label="From"
          prompt="Choose a User"
          options={
            Pc.Accounts.list_users()
            |> Enum.map(fn u -> {u.email, u.id} end)
            |> Enum.to_list()
          }
        />
        <.input
          field={@form[:to]}
          type="select"
          label="To"
          prompt="Choose a User"
          options={
            Pc.Accounts.list_users()
            |> Enum.map(fn u -> {u.email, u.id} end)
            |> Enum.to_list()
          }
        />
        <.input field={@form[:sequence]} type="number" label="Sequence" />
        <.input field={@form[:active]} type="checkbox" label="Active" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Link</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{link: link} = assigns, socket) do
    changeset = Chains.change_link(link)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"link" => link_params}, socket) do
    changeset =
      socket.assigns.link
      |> Chains.change_link(link_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"link" => link_params}, socket) do
    save_link(socket, socket.assigns.action, link_params)
  end

  defp save_link(socket, :edit, link_params) do
    case Chains.update_link(socket.assigns.link, link_params) do
      {:ok, link} ->
        notify_parent({:saved, link})

        {:noreply,
         socket
         |> put_flash(:info, "Link updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_link(socket, :new, link_params) do
    case Chains.create_link(link_params) do
      {:ok, link} ->
        notify_parent({:saved, link})

        {:noreply,
         socket
         |> put_flash(:info, "Link created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
