defmodule PcWeb.PrecantsLive.FormComponent do
  use PcWeb, :live_component

  alias Pc.Accounts

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage precants records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="precants-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input
          field={@form[:user]}
          type="select"
          label="Precant"
          prompt="Choose a Precant"
          options={
            Pc.Accounts.list_users()
            |> Enum.map(fn u -> {u.email, u.id} end)
            |> Enum.to_list()
          }
        />
        <.input
          field={@form[:group]}
          type="select"
          label="Group"
          prompt="Choose a Group"
          options={
            Pc.Chains.list_groups()
            |> Enum.map(fn g -> {g.name, g.id} end)
            |> Enum.to_list()
          }
        />

        <:actions>
          <.button phx-disable-with="Saving...">Save Precants</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{precants: precants} = assigns, socket) do
    changeset = Accounts.change_precants(precants)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"precants" => precants_params}, socket) do
    changeset =
      socket.assigns.precants
      |> Accounts.change_precants(precants_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"precants" => precants_params}, socket) do
    save_precants(socket, socket.assigns.action, precants_params)
  end

  defp save_precants(socket, :edit, precants_params) do
    case Accounts.update_precants(socket.assigns.precants, precants_params) do
      {:ok, precants} ->
        notify_parent({:saved, precants})

        {:noreply,
         socket
         |> put_flash(:info, "Precants updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_precants(socket, :new, precants_params) do
    case Accounts.create_precants(precants_params) do
      {:ok, precants} ->
        notify_parent({:saved, precants})

        {:noreply,
         socket
         |> put_flash(:info, "Precants created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
