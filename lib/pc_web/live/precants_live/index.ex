defmodule PcWeb.PrecantsLive.Index do
  use PcWeb, :live_view

  alias Pc.Accounts
  alias Pc.Accounts.Precants

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :precants_collection, Accounts.list_precants())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Precants")
    |> assign(:precants, Accounts.get_precants!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Precants")
    |> assign(:precants, %Precants{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Precants")
    |> assign(:precants, nil)
  end

  @impl true
  def handle_info({PcWeb.PrecantsLive.FormComponent, {:saved, precants}}, socket) do
    {:noreply, stream_insert(socket, :precants_collection, precants)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    precants = Accounts.get_precants!(id)
    {:ok, _} = Accounts.delete_precants(precants)

    {:noreply, stream_delete(socket, :precants_collection, precants)}
  end
end
