defmodule PcWeb.ChainLive.Index do
  use PcWeb, :live_view

  alias Pc.Chains
  alias Pc.Chains.Chain

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :chains, Chains.list_chains())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Chain")
    |> assign(:chain, Chains.get_chain!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Chain")
    |> assign(:chain, %Chain{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Chains")
    |> assign(:chain, nil)
  end

  @impl true
  def handle_info({PcWeb.ChainLive.FormComponent, {:saved, chain}}, socket) do
    {:noreply, stream_insert(socket, :chains, chain)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    chain = Chains.get_chain!(id)
    {:ok, _} = Chains.delete_chain(chain)

    {:noreply, stream_delete(socket, :chains, chain)}
  end
end
