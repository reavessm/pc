defmodule PcWeb.ChainLive.Show do
  use PcWeb, :live_view

  alias Pc.Chains

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:chain, Chains.get_chain!(id))}
  end

  defp page_title(:show), do: "Show Chain"
  defp page_title(:edit), do: "Edit Chain"
end
