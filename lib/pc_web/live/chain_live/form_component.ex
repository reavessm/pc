defmodule PcWeb.ChainLive.FormComponent do
  use PcWeb, :live_component

  alias Pc.Chains

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage chain records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="chain-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input
          field={@form[:group]}
          type="select"
          label="Group"
          prompt="Choose a group"
          options={
            Chains.list_groups()
            |> Enum.map(fn g -> {g.name, g.id} end)
            |> Enum.to_list()
          }
        />
        <.input field={@form[:name]} type="text" label="Name" placeholder={Date.utc_today()} />
        <.input field={@form[:topics]} type="text" label="Topics" />
        <%= if @action == :edit do %>
          <.input
            field={@form[:status]}
            type="select"
            label="Status"
            prompt="Choose a value"
            options={Ecto.Enum.values(Pc.Chains.Chain, :status)}
          />
        <% end %>
        <:actions>
          <.button phx-disable-with="Saving...">Save Chain</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{chain: chain} = assigns, socket) do
    changeset = Chains.change_chain(chain)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"chain" => chain_params}, socket) do
    changeset =
      socket.assigns.chain
      |> Chains.change_chain(chain_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"chain" => chain_params}, socket) do
    save_chain(socket, socket.assigns.action, chain_params)
  end

  defp save_chain(socket, :edit, chain_params) do
    case Chains.update_chain(socket.assigns.chain, chain_params) do
      {:ok, chain} ->
        notify_parent({:saved, chain})

        {:noreply,
         socket
         |> put_flash(:info, "Chain updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_chain(socket, :new, chain_params) do
    # Default status to 'created' when newly created, but allow changing on edit
    case Chains.create_chain(Map.put(chain_params, "status", "created")) do
      {:ok, chain} ->
        notify_parent({:saved, chain})

        {:noreply,
         socket
         |> put_flash(:info, "Chain created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
