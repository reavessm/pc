defmodule Pc.Accounts.Precants do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "precants" do
    field :group, :binary_id
    field :user, :binary_id

    timestamps()
  end

  @doc false
  def changeset(precants, attrs) do
    precants
    |> cast(attrs, [:group, :user])
    |> validate_required([:group, :user])
    |> unique_constraint(:precant_per_group, name: :precant_per_group)
  end
end
