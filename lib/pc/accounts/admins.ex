defmodule Pc.Accounts.Admins do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "admins" do

    field :user_id, :binary_id

    timestamps()
  end

  @doc false
  def changeset(admins, attrs) do
    admins
    |> cast(attrs, [])
    |> validate_required([])
  end
end
