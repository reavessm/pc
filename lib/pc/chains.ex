defmodule Pc.Chains do
  @moduledoc """
  The Chains context.
  """

  import Ecto.Query, warn: false
  alias Pc.Repo

  alias Pc.Chains.Group

  @doc """
  Returns the list of groups.

  ## Examples

      iex> list_groups()
      [%Group{}, ...]

  """
  def list_groups do
    Repo.all(Group)
  end

  @doc """
  Gets a single group.

  Raises `Ecto.NoResultsError` if the Group does not exist.

  ## Examples

      iex> get_group!(123)
      %Group{}

      iex> get_group!(456)
      ** (Ecto.NoResultsError)

  """
  def get_group!(id), do: Repo.get!(Group, id)

  @doc """
  Creates a group.

  ## Examples

      iex> create_group(%{field: value})
      {:ok, %Group{}}

      iex> create_group(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_group(attrs \\ %{}) do
    %Group{}
    |> Group.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a group.

  ## Examples

      iex> update_group(group, %{field: new_value})
      {:ok, %Group{}}

      iex> update_group(group, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_group(%Group{} = group, attrs) do
    group
    |> Group.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a group.

  ## Examples

      iex> delete_group(group)
      {:ok, %Group{}}

      iex> delete_group(group)
      {:error, %Ecto.Changeset{}}

  """
  def delete_group(%Group{} = group) do
    Repo.delete(group)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking group changes.

  ## Examples

      iex> change_group(group)
      %Ecto.Changeset{data: %Group{}}

  """
  def change_group(%Group{} = group, attrs \\ %{}) do
    Group.changeset(group, attrs)
  end

  alias Pc.Chains.Chain

  @doc """
  Returns the list of chains.

  ## Examples

      iex> list_chains()
      [%Chain{}, ...]

  """
  def list_chains do
    Repo.all(Chain)
  end

  @doc """
  Gets a single chain.

  Raises `Ecto.NoResultsError` if the Chain does not exist.

  ## Examples

      iex> get_chain!(123)
      %Chain{}

      iex> get_chain!(456)
      ** (Ecto.NoResultsError)

  """
  def get_chain!(id), do: Repo.get!(Chain, id)

  @doc """
  Creates a chain.

  ## Examples

      iex> create_chain(%{field: value})
      {:ok, %Chain{}}

      iex> create_chain(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_chain(attrs \\ %{}) do
    %Chain{}
    |> Chain.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a chain.

  ## Examples

      iex> update_chain(chain, %{field: new_value})
      {:ok, %Chain{}}

      iex> update_chain(chain, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_chain(%Chain{} = chain, attrs) do
    chain
    |> Chain.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a chain.

  ## Examples

      iex> delete_chain(chain)
      {:ok, %Chain{}}

      iex> delete_chain(chain)
      {:error, %Ecto.Changeset{}}

  """
  def delete_chain(%Chain{} = chain) do
    Repo.delete(chain)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking chain changes.

  ## Examples

      iex> change_chain(chain)
      %Ecto.Changeset{data: %Chain{}}

  """
  def change_chain(%Chain{} = chain, attrs \\ %{}) do
    Chain.changeset(chain, attrs)
  end

  alias Pc.Chains.Link

  @doc """
  Returns the list of links.

  ## Examples

      iex> list_links()
      [%Link{}, ...]

  """
  def list_links do
    Repo.all(Link)
  end

  @doc """
  Gets a single link.

  Raises `Ecto.NoResultsError` if the Link does not exist.

  ## Examples

      iex> get_link!(123)
      %Link{}

      iex> get_link!(456)
      ** (Ecto.NoResultsError)

  """
  def get_link!(id), do: Repo.get!(Link, id)

  @doc """
  Creates a link.

  ## Examples

      iex> create_link(%{field: value})
      {:ok, %Link{}}

      iex> create_link(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_link(attrs \\ %{}) do
    %Link{}
    |> Link.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a link.

  ## Examples

      iex> update_link(link, %{field: new_value})
      {:ok, %Link{}}

      iex> update_link(link, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_link(%Link{} = link, attrs) do
    link
    |> Link.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a link.

  ## Examples

      iex> delete_link(link)
      {:ok, %Link{}}

      iex> delete_link(link)
      {:error, %Ecto.Changeset{}}

  """
  def delete_link(%Link{} = link) do
    Repo.delete(link)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking link changes.

  ## Examples

      iex> change_link(link)
      %Ecto.Changeset{data: %Link{}}

  """
  def change_link(%Link{} = link, attrs \\ %{}) do
    Link.changeset(link, attrs)
  end
end
