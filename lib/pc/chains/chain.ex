defmodule Pc.Chains.Chain do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "chains" do
    field :name, :string
    field :status, Ecto.Enum, values: [:created, :started, :finished]
    field :topics, :string
    field :group, :binary_id
    field :current_link, :binary_id

    timestamps()
  end

  @doc false
  def changeset(chain, attrs) do
    chain
    |> cast(attrs, [:name, :topics, :status, :group, :current_link])
    |> validate_required([:name, :topics, :status, :group])
  end
end
