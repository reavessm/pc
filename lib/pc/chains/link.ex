defmodule Pc.Chains.Link do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "links" do
    field :active, :boolean, default: false
    field :sequence, :integer
    field :chain, :binary_id
    field :from, :binary_id
    field :to, :binary_id

    timestamps()
  end

  @doc false
  def changeset(link, attrs) do
    link
    |> cast(attrs, [:sequence, :active])
    |> validate_required([:sequence, :active])
  end
end
