defmodule Pc.Repo do
  use Ecto.Repo,
    otp_app: :pc,
    adapter: Ecto.Adapters.SQLite3
end
