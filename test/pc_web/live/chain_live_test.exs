defmodule PcWeb.ChainLiveTest do
  use PcWeb.ConnCase

  import Phoenix.LiveViewTest
  import Pc.ChainsFixtures

  @moduletag :ui

  @create_attrs %{name: "some name", status: :created, topics: "some topics"}
  @update_attrs %{name: "some updated name", status: :started, topics: "some updated topics"}
  @invalid_attrs %{name: nil, status: nil, topics: nil}

  defp create_chain(_) do
    chain = chain_fixture()
    %{chain: chain}
  end

  describe "Index" do
    setup [:create_chain]

    test "lists all chains", %{conn: conn, chain: chain} do
      {:ok, _index_live, html} = live(conn, ~p"/chains")

      assert html =~ "Listing Chains"
      assert html =~ chain.name
    end

    test "saves new chain", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/chains")

      assert index_live |> element("a", "New Chain") |> render_click() =~
               "New Chain"

      assert_patch(index_live, ~p"/chains/new")

      assert index_live
             |> form("#chain-form", chain: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#chain-form", chain: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/chains")

      html = render(index_live)
      assert html =~ "Chain created successfully"
      assert html =~ "some name"
    end

    test "updates chain in listing", %{conn: conn, chain: chain} do
      {:ok, index_live, _html} = live(conn, ~p"/chains")

      assert index_live |> element("#chains-#{chain.id} a", "Edit") |> render_click() =~
               "Edit Chain"

      assert_patch(index_live, ~p"/chains/#{chain}/edit")

      assert index_live
             |> form("#chain-form", chain: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#chain-form", chain: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/chains")

      html = render(index_live)
      assert html =~ "Chain updated successfully"
      assert html =~ "some updated name"
    end

    test "deletes chain in listing", %{conn: conn, chain: chain} do
      {:ok, index_live, _html} = live(conn, ~p"/chains")

      assert index_live |> element("#chains-#{chain.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#chains-#{chain.id}")
    end
  end

  describe "Show" do
    setup [:create_chain]

    test "displays chain", %{conn: conn, chain: chain} do
      {:ok, _show_live, html} = live(conn, ~p"/chains/#{chain}")

      assert html =~ "Show Chain"
      assert html =~ chain.name
    end

    test "updates chain within modal", %{conn: conn, chain: chain} do
      {:ok, show_live, _html} = live(conn, ~p"/chains/#{chain}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Chain"

      assert_patch(show_live, ~p"/chains/#{chain}/show/edit")

      assert show_live
             |> form("#chain-form", chain: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#chain-form", chain: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/chains/#{chain}")

      html = render(show_live)
      assert html =~ "Chain updated successfully"
      assert html =~ "some updated name"
    end
  end
end
