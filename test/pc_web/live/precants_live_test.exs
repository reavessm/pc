defmodule PcWeb.PrecantsLiveTest do
  use PcWeb.ConnCase

  import Phoenix.LiveViewTest
  import Pc.AccountsFixtures

  @moduletag :ui

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  defp create_precants(_) do
    precants = precants_fixture()
    %{precants: precants}
  end

  describe "Index" do
    setup [:create_precants]

    test "lists all precants", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/precants")

      assert html =~ "Listing Precants"
    end

    test "saves new precants", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/precants")

      assert index_live |> element("a", "New Precants") |> render_click() =~
               "New Precants"

      assert_patch(index_live, ~p"/precants/new")

      assert index_live
             |> form("#precants-form", precants: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#precants-form", precants: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/precants")

      html = render(index_live)
      assert html =~ "Precants created successfully"
    end

    test "updates precants in listing", %{conn: conn, precants: precants} do
      {:ok, index_live, _html} = live(conn, ~p"/precants")

      assert index_live |> element("#precants-#{precants.id} a", "Edit") |> render_click() =~
               "Edit Precants"

      assert_patch(index_live, ~p"/precants/#{precants}/edit")

      assert index_live
             |> form("#precants-form", precants: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#precants-form", precants: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/precants")

      html = render(index_live)
      assert html =~ "Precants updated successfully"
    end

    test "deletes precants in listing", %{conn: conn, precants: precants} do
      {:ok, index_live, _html} = live(conn, ~p"/precants")

      assert index_live |> element("#precants-#{precants.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#precants-#{precants.id}")
    end
  end

  describe "Show" do
    setup [:create_precants]

    test "displays precants", %{conn: conn, precants: precants} do
      {:ok, _show_live, html} = live(conn, ~p"/precants/#{precants}")

      assert html =~ "Show Precants"
    end

    test "updates precants within modal", %{conn: conn, precants: precants} do
      {:ok, show_live, _html} = live(conn, ~p"/precants/#{precants}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Precants"

      assert_patch(show_live, ~p"/precants/#{precants}/show/edit")

      assert show_live
             |> form("#precants-form", precants: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#precants-form", precants: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/precants/#{precants}")

      html = render(show_live)
      assert html =~ "Precants updated successfully"
    end
  end
end
