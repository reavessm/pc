defmodule Pc.ChainsTest do
  use Pc.DataCase

  alias Pc.Chains

  @moduletag :backend

  describe "groups" do
    alias Pc.Chains.Group

    import Pc.ChainsFixtures

    @invalid_attrs %{name: nil}

    test "list_groups/0 returns all groups" do
      group = group_fixture()
      assert Chains.list_groups() == [group]
    end

    test "get_group!/1 returns the group with given id" do
      group = group_fixture()
      assert Chains.get_group!(group.id) == group
    end

    test "create_group/1 with valid data creates a group" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Group{} = group} = Chains.create_group(valid_attrs)
      assert group.name == "some name"
    end

    test "create_group/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chains.create_group(@invalid_attrs)
    end

    test "update_group/2 with valid data updates the group" do
      group = group_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Group{} = group} = Chains.update_group(group, update_attrs)
      assert group.name == "some updated name"
    end

    test "update_group/2 with invalid data returns error changeset" do
      group = group_fixture()
      assert {:error, %Ecto.Changeset{}} = Chains.update_group(group, @invalid_attrs)
      assert group == Chains.get_group!(group.id)
    end

    test "delete_group/1 deletes the group" do
      group = group_fixture()
      assert {:ok, %Group{}} = Chains.delete_group(group)
      assert_raise Ecto.NoResultsError, fn -> Chains.get_group!(group.id) end
    end

    test "change_group/1 returns a group changeset" do
      group = group_fixture()
      assert %Ecto.Changeset{} = Chains.change_group(group)
    end
  end

  describe "chains" do
    alias Pc.Chains.Chain

    import Pc.ChainsFixtures

    @invalid_attrs %{name: nil, status: nil, topics: nil}

    test "list_chains/0 returns all chains" do
      chain = chain_fixture()
      assert Chains.list_chains() == [chain]
    end

    test "get_chain!/1 returns the chain with given id" do
      chain = chain_fixture()
      assert Chains.get_chain!(chain.id) == chain
    end

    test "create_chain/1 with valid data creates a chain" do
      valid_attrs = %{
        name: "some name",
        status: :created,
        group: group_fixture().id,
        topics: "some topics"
      }

      assert {:ok, %Chain{} = chain} = Chains.create_chain(valid_attrs)
      assert chain.name == "some name"
      assert chain.status == :created
      assert chain.topics == "some topics"
    end

    test "create_chain/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chains.create_chain(@invalid_attrs)
    end

    test "update_chain/2 with valid data updates the chain" do
      chain = chain_fixture()
      update_attrs = %{name: "some updated name", status: :started, topics: "some updated topics"}

      assert {:ok, %Chain{} = chain} = Chains.update_chain(chain, update_attrs)
      assert chain.name == "some updated name"
      assert chain.status == :started
      assert chain.topics == "some updated topics"
    end

    test "update_chain/2 with invalid data returns error changeset" do
      chain = chain_fixture()
      assert {:error, %Ecto.Changeset{}} = Chains.update_chain(chain, @invalid_attrs)
      assert chain == Chains.get_chain!(chain.id)
    end

    test "delete_chain/1 deletes the chain" do
      chain = chain_fixture()
      assert {:ok, %Chain{}} = Chains.delete_chain(chain)
      assert_raise Ecto.NoResultsError, fn -> Chains.get_chain!(chain.id) end
    end

    test "change_chain/1 returns a chain changeset" do
      chain = chain_fixture()
      assert %Ecto.Changeset{} = Chains.change_chain(chain)
    end
  end

  describe "links" do
    alias Pc.Chains.Link

    import Pc.ChainsFixtures

    @invalid_attrs %{active: nil, sequence: nil}

    test "list_links/0 returns all links" do
      link = link_fixture()
      assert Chains.list_links() == [link]
    end

    test "get_link!/1 returns the link with given id" do
      link = link_fixture()
      assert Chains.get_link!(link.id) == link
    end

    test "create_link/1 with valid data creates a link" do
      valid_attrs = %{active: true, sequence: 42}

      assert {:ok, %Link{} = link} = Chains.create_link(valid_attrs)
      assert link.active == true
      assert link.sequence == 42
    end

    test "create_link/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chains.create_link(@invalid_attrs)
    end

    test "update_link/2 with valid data updates the link" do
      link = link_fixture()
      update_attrs = %{active: false, sequence: 43}

      assert {:ok, %Link{} = link} = Chains.update_link(link, update_attrs)
      assert link.active == false
      assert link.sequence == 43
    end

    test "update_link/2 with invalid data returns error changeset" do
      link = link_fixture()
      assert {:error, %Ecto.Changeset{}} = Chains.update_link(link, @invalid_attrs)
      assert link == Chains.get_link!(link.id)
    end

    test "delete_link/1 deletes the link" do
      link = link_fixture()
      assert {:ok, %Link{}} = Chains.delete_link(link)
      assert_raise Ecto.NoResultsError, fn -> Chains.get_link!(link.id) end
    end

    test "change_link/1 returns a link changeset" do
      link = link_fixture()
      assert %Ecto.Changeset{} = Chains.change_link(link)
    end
  end
end
