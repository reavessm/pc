defmodule Pc.ChainsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Pc.Chains` context.
  """

  @doc """
  Generate a group.
  """
  def group_fixture(attrs \\ %{}) do
    {:ok, group} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> Pc.Chains.create_group()

    group
  end

  @doc """
  Generate a chain.
  """
  def chain_fixture(attrs \\ %{}) do
    group = group_fixture()

    {:ok, chain} =
      attrs
      |> Enum.into(%{
        name: "some name",
        group: group.id,
        status: :created,
        topics: "some topics"
      })
      |> Pc.Chains.create_chain()

    chain
  end

  @doc """
  Generate a link.
  """
  def link_fixture(attrs \\ %{}) do
    {:ok, link} =
      attrs
      |> Enum.into(%{
        active: true,
        sequence: 42
      })
      |> Pc.Chains.create_link()

    link
  end
end
