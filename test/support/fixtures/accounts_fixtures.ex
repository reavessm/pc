defmodule Pc.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Pc.Accounts` context.
  """
  alias Pc.ChainsFixtures

  def unique_users_email, do: "users#{System.unique_integer()}@example.com"
  def valid_users_password, do: "hello world!"

  def valid_users_attributes(attrs \\ %{}) do
    Enum.into(attrs, %{
      email: unique_users_email(),
      password: valid_users_password()
    })
  end

  def users_fixture(attrs \\ %{}) do
    {:ok, users} =
      attrs
      |> valid_users_attributes()
      |> Pc.Accounts.register_users()

    users
  end

  def extract_users_token(fun) do
    {:ok, captured_email} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token | _] = String.split(captured_email.text_body, "[TOKEN]")
    token
  end

  @doc """
  Generate a precants.
  """
  def precants_fixture(attrs \\ %{}) do
    {:ok, precants} =
      attrs
      |> Enum.into(%{
        group: ChainsFixtures.group_fixture().id,
        user: users_fixture().id
      })
      |> Pc.Accounts.create_precants()

    precants
  end
end
